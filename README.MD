# Todo App fullstack

This is a todo app fullstack contains 3 front-end today main frameworks
and backend will be json-server for pre mock backend, finally will integate with nodejs, express, mongodb.
and graphql api for react

## Folder strcuture

- client-angular is angular client side front-end folder  
<http://localhost:4200>

- client-react is react client side front-end folder  
<http://localhost:3000>

- client-vue is vue client side front-end folder  
<http://localhost:8080>

- server is backend folder using nodejs, express, mongodb  
<http://localhost:5000/api/v1>  
<http://localhost:5000/docs/swagger>

- server_ql backend folder using node, express, graphql
<http://localhost:5001/api/v1>

- .nvmc is node version for my app

- docs

> backend postman json files  
> backend database UML: <https://app.lucidchart.com/documents/edit/5cdcf795-82ed-41b3-a0bb-f1764bbf3a52/0_0?beaconFlowId=D944DC306A52FE1B>  
> Api mapping folder images

- Additional info

> UML join view only link: <https://app.lucidchart.com/invitations/accept/c7cf69ca-90e2-491d-87f3-dda2782fa689>
> trello: <https://trello.com/b/IHYxgUdU/todo-fullstack-project>  
> trello join link: <https://trello.com/invite/b/IHYxgUdU/df7abaf6bf3b1e84b599e246e14d8d78/todo-fullstack-project>

- git commit rule:

> Front_Angular: xxxxxx  
> Front_React: xxxxxx  
> Front_Vue: xxxxxx  
> Backend_MEN: xxxxxx  
> Backend_GQL: xxxxxx  
> General: xxxxxx  

- roles

> If login as admin, will check the admin dashboard page (example: iversonLv 123123123123)
> If login as normal user, will check the user dashboard page (example: iversonLv_admin 123123123123)

- Check list
- Front_Angular 2021.11.30 - Use BehaviorSubect to real time update todoInfos accross the whole app
- Front_Angular 2022.10.02 - Use BehaviorSubect to real time update profile accross the whole app
