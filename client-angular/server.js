const express = require('express')
const app = express()
const PORT = process.env.PORT || 4201

console.log(__dirname)
app.use(express.static(__dirname + '/dist'))
app.get('/*', (req, res) => {
  res.sendFile(__dirname + '/dist/index.html')
})

app.listen(PORT, () => {
  console.log(`Full stack todo app: Angular app is running on port ${PORT}`)
})


// const express = require('express')
// const app = express()
// const PORT = process.env.PORT || 4201
// const publicweb = process.env.PUBLICWEB || './dist'

// app.use(express.static(publicweb))
// app.get('*', (req, res) => {
//   res.sendFile(`index.html`, {root: publicweb})
// })

// app.listen(PORT, () => {
//   console.log(`Full stack todo app: Angular app is running on port ${PORT}`)
// })