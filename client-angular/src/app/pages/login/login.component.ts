import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';

import { IUser } from '../../models/user';
import { AuthService } from '../../services/auth.service';

import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.scss']
})
export class LoginComponent implements OnInit {
  user: IUser = null;

  // loading, errorMessage when action form
  loading = false;
  errorMessage: any = null;

  loginForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    public authService: AuthService,
    public router: Router,
  ) {
  }

  get f(): any { return this.loginForm.controls; }

  ngOnInit(): void {
    // backend Joi
    // username: Joi.string().regex(/(^[a-zA-Z0-9_]*$)/).min(2).max(30).required(),
    // password: Joi.string().trim().min(10).required(),
    this.loginForm = this.formBuilder.group({
      username: ['', Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30),
        Validators.pattern(/(^[a-zA-Z0-9_]*$)/)
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(10),
      ])],
    });
  }

  login(user: IUser): any {
    this.loading = true;
    this.errorMessage = null;

    this.authService.loginUser(user).subscribe(data => {
      this.loading = false;
      this.loginForm.reset();
      localStorage.setItem('token', data.token);

      const helper = new JwtHelperService(); // for decode token
      const decodedToken =  helper.decodeToken(data.token); // for decode token

      if (decodedToken.roles[0] === 'admin') {
        this.router.navigate(['/dashboard']);
      } else {
        this.router.navigate(['/todos']);
      }
    }, error => {
      console.log(error);
      this.errorMessage = error;
      this.loading = false;
    });
  }

}
