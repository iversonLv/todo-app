import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Location } from '@angular/common';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { TodoService } from '../../services/todo.service';
import { CategoryService } from '../../services/category.service';

import { ITodo } from '../../models/todo';
import { ICategory } from '../../models/category';
import { ModalRemoveComponent } from 'src/app/components/modal-remove/modal-remove.component';

@Component({
  selector: 'app-todo-edit',
  templateUrl: './todo-edit.component.html',
  styleUrls: ['./todo-edit.component.scss']
})
export class TodoEditComponent implements OnInit {
  token = localStorage.getItem('token');
  // removeTodoModal = false;
  todo: ITodo = null;
  categories: ICategory[] = null;

  // loading, errorMessage when action form
  loading = false;
  errorMessage: any = null;

  updateTodoForm: FormGroup;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private location: Location,
    private todoService: TodoService,
    private categoryService: CategoryService,
    private formBuilder: FormBuilder,
    private modalService: NgbModal,
    ) {
      this.updateTodoForm = this.formBuilder.group({
        title: ['', Validators.required],
        category: ['', Validators.required],
        isComplete: ['', Validators.required],
        startEndRange: [[new Date(), new Date()], Validators.required]
      });

    }

  get f(): any { return this.updateTodoForm.controls; }

  ngOnInit(): void {
    this.getCategories();
    this.getTodo();
  }

  backPreviousPage(): void {
    this.location.back();
  }

  getTodo(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.todoService.getTodo(id, this.token).subscribe(data => {
      this.todo = data;
      const { title, isComplete, start, end, category } = data;
      const startEndRange = [start, end];
      this.updateTodoForm.setValue({ title, isComplete, startEndRange, category });
    }, error => {
      console.log(error);
    });
  }

  getCategories(): void {
    // this.categoryService.getCategories().subscribe(data => this.categories = data);
    this.categories = this.categoryService.getCategories();
  }

  updateTodo(todo: any): void {
    this.loading = true;
    this.errorMessage = null;

    // grab form necessary data, ES6 feature
    const { title, category, startEndRange, isComplete } = todo;

    const updateTodo: ITodo = {
      title,
      category,
      isComplete,
      start: startEndRange[0],
      end: startEndRange[1],
    };
    this.todoService.getTodoInfos(this.token);
    this.todoService.updateTodo(this.todo._id, updateTodo, this.token).subscribe(data => {
      console.log(`Updated todo ${data._id}`);
      this.loading = false;
    }, error => {
      console.log(error);
      this.errorMessage = error;
      this.loading = false;
    });
  }

  // show the remove modal
  // and show the remove todo modal
  showRemoveTodoModal(todo: ITodo): void {
    const modalRef = this.modalService.open(ModalRemoveComponent, { centered: true });
    // grab the remove todo data and pass to the modal
    modalRef.componentInstance.currentRemoveTodo = todo;
    // modal response the result which is the remove id: currentRemoveTodo['_id']
    modalRef.result.then((result: string) => {
      if (result !== 'Close') {
        this.removeTodo(result);
      }
    }, (reason) => {
      console.log(reason);
    });
  }

  // remove todo
  removeTodo(id: string): any {
    this.todoService.deleteTodo(id, this.token).subscribe(data => {
      this.todoService.getTodoInfos(this.token);
      // research
      // get todo again
      console.log(data);
    }, error => {
      console.log(error);
    });
    this.router.navigate(['/todos']);
  }

}
