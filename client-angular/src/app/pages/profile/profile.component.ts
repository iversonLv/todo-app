import { Component, OnInit, ViewChild, ElementRef } from '@angular/core';

import { AuthService } from '../../services/auth.service';
import { IUser } from '../../models/user';
import { MustMatch } from '../../shared/confirm-password';

import { FormGroup, FormBuilder, Validators } from '@angular/forms';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.component.html',
  styleUrls: ['./profile.component.scss']
})
export class ProfileComponent implements OnInit {
  @ViewChild('avatarFileInputElementRef') avatarFileInputElementRef: ElementRef;
  private token = localStorage.getItem('token');

  me: IUser = null;
  image = null;

  // loading, errorMessage when action form
  loading = false;
  errorMessage: any = null;

  updateProfileForm: FormGroup;

  constructor(
    private authService: AuthService,
    private formBuilder: FormBuilder,
  ) { }

  get f(): any {return this.updateProfileForm.controls; }

  ngOnInit(): void {
    this.getProfile();

    // backend Joi
    // username: Joi.string().regex(/(^[a-zA-Z0-9_]*$)/).min(2).max(30).required(),
    // password: Joi.string().trim().min(10).required(),
    // avatar accepte file type: jpg|jpeg|png|gif
    this.updateProfileForm = this.formBuilder.group({
      avatar: [''],
      username: ['', Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30),
        Validators.pattern(/(^[a-zA-Z0-9_]*$)/)
      ])],
      currentpassword: ['', Validators.compose([
        Validators.required,
        Validators.minLength(10),
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(10),
      ])],
      confirmpassword: ['', Validators.required],
    }, {
      validator: MustMatch('password', 'confirmpassword')
    });
  }

  // get profile
  getProfile(): void {
    this.authService.meInfo.subscribe(me => {
      this.me = me;
      this.updateProfileForm.controls.username.setValue(this.me.username);
    }, error => {
      console.log(error);
    });
    this.authService.getProfile(this.token);
  }


  // select image and get the file data
  selectImage(event): void {
    if (event.target.files.length > 0) {
      const file = event.target.files[0];
      this.updateProfileForm.get('avatar').setValue(file);
    }
  }

  // update profile
  updateProfile(userInfo: any): void {
    const formData = new FormData();

    this.loading = true;
    this.errorMessage = null;

    // grab form necessary data, ES6 feature
    const { username, password, avatar, currentpassword } = userInfo;

    formData.append('avatar', avatar);
    formData.append('currentpassword', currentpassword);
    formData.append('username', username);
    formData.append('password', password);
    // integrate with backend service
    this.authService.updateMe(formData, this.token).subscribe(data => {
      console.log(data);
      this.loading = false;
      this.getProfile();
      // reset file input value to null
      this.avatarFileInputElementRef.nativeElement.value = '';
      // patch value, exception for username
      this.updateProfileForm.patchValue({
        avatar: '',
        currentpassword: '',
        password: '',
        confirmpassword: '',
      });
    }, error => {
      console.log(error);
      this.errorMessage = error;
      this.loading = false;
    });

  }

}
