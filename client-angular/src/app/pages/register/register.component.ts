import { Component, OnInit, ViewChild, ElementRef, AfterContentInit } from '@angular/core';
import { Router } from '@angular/router';
import { FormGroup, Validators, FormBuilder } from '@angular/forms';

import { MustMatch } from '../../shared/confirm-password';
import { IUser } from '../../models/user';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.scss']
})
export class RegisterComponent implements OnInit, AfterContentInit {
  user: IUser = null;

  @ViewChild('registerInputElementRef') registerInputElementRef: ElementRef;

  // loading, errorMessage when action form
  loading = false;
  errorMessage: any = null;

  registerForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private authService: AuthService,
    private router: Router,
  ) { }

  get f(): any { return this.registerForm.controls; }

  ngOnInit(): void {
    // backend Joi
    // username: Joi.string().regex(/(^[a-zA-Z0-9_]*$)/).min(2).max(30).required(),
    // password: Joi.string().trim().min(10).required(),
    this.registerForm = this.formBuilder.group({
      username: ['', Validators.compose([
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30),
        Validators.pattern(/(^[a-zA-Z0-9_]*$)/)
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.minLength(10),
      ])],
      confirmpassword: ['', Validators.required],
    }, {
      validator: MustMatch('password', 'confirmpassword')
    });
  }

  ngAfterContentInit(): void {
    setTimeout(() => {
      this.registerInputElementRef.nativeElement.focus();
    }, 0);
  }

  register(user: IUser): void {
    this.loading = true;
    this.errorMessage = null;
    // grab form necessary data, ES6 feature
    const { username, password } = user;

    const newUser: IUser = { username, password };
    this.authService.createUser(newUser).subscribe(data => {
      console.log(data);
      this.loading = false;
      this.registerForm.reset();
      localStorage.setItem('token', data.token);
      this.router.navigate(['/todos']);
    }, error => {
      console.log(error);
      this.errorMessage = error;
      this.loading = false;
    });
  }



}
