import { Component, ElementRef, OnInit, ViewChild, ViewChildren } from '@angular/core';
import { TodoService } from 'src/app/services/todo.service';
import { InfoService } from '../../services/info.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.scss']
})
export class AdminDashboardComponent implements OnInit {
  @ViewChild('startEndRangeRef') startEndRangeRef: ElementRef;

  token = localStorage.getItem('token');

  allNonAdminUsers;
  totalData;
  topTenUsers;

  params = {
    after: '',
    before: '',
    category: '',
    isComplete: ''
  };

  infoParams = {
    after: '',
    before: '',
    isComplete: ''
  };

  // card state
  category = ['total'];

  // tab today|month|year
  currentFilterRange = '';
  // calendar range model
  startEndRangeModel = [];
  // tab isComplete|Complete
  currentFilterisComplete = '';

  constructor(
    private infoService: InfoService,
    private todoService: TodoService
  ) {}


  ngOnInit(): void {

    delete this.params.after;
    delete this.params.before;
    delete this.params.category;
    delete this.params.isComplete;

    delete this.infoParams.after;
    delete this.infoParams.before;
    delete this.infoParams.isComplete;

    this.getTopTen(this.token, {...this.params});
    this.getTodoInfos(this.token, { ...this.infoParams });
    this.getUsers(this.token);
  }

  getTopTen(token: string, params?): any {
    this.infoService.getTopTen(token, params).subscribe(data => {
      this.topTenUsers = data;
    }, error => {
      console.log(error);
    });
  }

  getTodoInfos(token: string, params?): void {
    this.todoService.getTodoInfos(token, params);
    this.todoService.todoInfos.subscribe(data => this.totalData = data);
    // this.todoService.getTodoInfos(token, params).subscribe(data => {
    //   this.totalData = data;
    // });
  }

  getUsers(token): void {
    this.infoService.getUsers(token).subscribe(data => {
      this.allNonAdminUsers = data.allNonAdminUsers;
    }, error => {
      console.log(error);
    });
  }

  // get current card and filter category
  getCurrentCardEmit(e): any {
    // set selected css for the select cards
    if (!this.category.includes(e)) {
      this.category.push(e);
    } else {
      if (!this.category.includes('total')) {
        this.category = this.category.filter(i =>  i !== e );
      }
    }

    if (e !== 'total') {
      // if the array is empty, will reset to total
      if (this.category.length === 0) {
        this.category.push('total');
        this.category = this.category.filter(i =>  i === 'total' );

        // reset the category
        delete this.params.category;
        console.log(this.params);
        this.getTopTen(this.token, { ...this.params });
      }
      // if array with all categories, will reset to total as well
      else if (this.category.includes('Daily life') && this.category.includes('Work') && this.category.includes('Entertaiment')) {
        this.category.push('total');
        this.category = this.category.filter(i =>  i === 'total' );
        // reset the category
        delete this.params.category;
        console.log(this.params);
        this.getTopTen(this.token, { ...this.params });
      } else {
        this.category = this.category.filter(i =>  i !== 'total' );
        const cat = this.category.join(',');
        this.params = {
          ...this.params,
          category: cat
        };
        console.log(this.params);
        this.getTopTen(this.token, { ...this.params });
      }

    } else {
      this.category = this.category.filter(i =>  i === 'total' );
      // reset the category
      delete this.params.category;
      console.log(this.params);
      this.getTopTen(this.token, { ...this.params });
    }
  }

  // get isComplete|complete tab data
  getisComplete(e): any {
    const isComplete = e.target.value;
    this.params = {
      ...this.params,
      isComplete
    };
    this.infoParams = {
      ...this.infoParams,
      isComplete
    };
    console.log(this.params);
    this.getTopTen(this.token, { ...this.params });
    this.getTodoInfos(this.token, { ...this.infoParams });
  }

  // get range tab data
  getRange(e): any {
    let after = '';
    let before = '';
    const value = e.target.value;
    const thirtyDayMonth = [1, 3, 4, 5, 6, 10, 12];

    const thisYear = new Date().getFullYear();
    const thisMonth = new Date().getMonth() + 1;
    const today = new Date().getDate();

    const thisYearStart = thisYear + '-01-01';
    const thisYearEnd = thisYear + '-12-31';

    const todayStart = thisYear + '-' + thisMonth + '-' + today;
    const todayEnd = thisYear + '-' + thisMonth + '-' + today;

    const thisMonthStart = thisYear + '-' + thisMonth + '-01';

    let thisMonthEnd;
    if (thirtyDayMonth.includes(thisMonth) ) {
      thisMonthEnd = thisYear + '-' + thisMonth +  '-31';
    } else if (thisMonth === 2) {
      if ((thisYear % 4 === 0 && thisYear % 100 !== 0) || thisYear % 400 === 0) {
        thisMonthEnd = thisYear + '-' + thisMonth +  '-29';
      } else {
        thisMonthEnd = thisYear + '-' + thisMonth +  '-28';
      }
    } else {
      thisMonthEnd = thisYear + '-' + thisMonth +  '-30';
    }

    if (value === 'today') {
      after = todayStart;
      before = todayEnd;
    } else if (value === 'month') {
      after = thisMonthStart;
      before = thisMonthEnd;
    } else if (value === 'year') {
      after = thisYearStart;
      before = thisYearEnd;
    } else {
      // reset the after, before
      delete this.params.after;
      delete this.params.before;
    }

    // populdate the day range to calendar
    this.startEndRangeModel = [new Date(after + 'T00:00'), new Date(before + 'T23:59')];
    this.params = {
      ...this.params,
      after, before
    };
    this.infoParams = {
      ...this.infoParams,
      after, before
    };
    console.log(this.params);
    this.getTopTen(this.token, { ...this.params });
    this.getTodoInfos(this.token, { ...this.infoParams });
  }

  // mock reset
  resetCurrentFilterRange(): any {
    // rest isComplete|Complete tab
    this.currentFilterisComplete = '';
    // reset today|month|year tab
    this.currentFilterRange = '';
    // reset calendar range
    this.startEndRangeRef.nativeElement.value = '';
    this.startEndRangeModel = [];

    // reset params after/before data
    delete this.params.after;
    delete this.params.before;
    delete this.params.isComplete;
    delete this.infoParams.after;
    delete this.infoParams.before;
    delete this.infoParams.isComplete;
    this.getTopTen(this.token, { ...this.params });
    this.getTodoInfos(this.token, { ...this.infoParams });
  }

  // date range
  dateTimeChange(e): any {
    // reset today|month|year tab
    this.currentFilterRange = '';
    if (e.value[0] !== null && e.value[1] !== null) {
      const after = this.dateFormat(e.value[0]);
      const before = this.dateFormat(e.value[1]);
      this.params = {
        ...this.params,
        after, before
      };
      this.infoParams = {
        ...this.infoParams,
        after, before
      };
      this.getTopTen(this.token, { ...this.params });
      this.getTodoInfos(this.token, { ...this.infoParams });
    }
  }

  // TODO: validate date

  dateFormat(date): any {
    // 2015-06-13 15:04
    // 2020-10-29T14:12:31.000Z
    date = date.toISOString();
    date = date.slice(0, 16);
    return date;
  }

}
