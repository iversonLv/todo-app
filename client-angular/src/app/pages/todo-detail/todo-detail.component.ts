import { Component, OnInit } from '@angular/core';
import { Location } from '@angular/common';
import { ActivatedRoute, Router } from '@angular/router';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

import { TodoService } from '../../services/todo.service';

import { ITodo } from '../../models/todo';

import { ModalRemoveComponent } from '../../components/modal-remove/modal-remove.component';

@Component({
  selector: 'app-todo-detail',
  templateUrl: './todo-detail.component.html',
  styleUrls: ['./todo-detail.component.scss']
})
export class TodoDetailComponent implements OnInit {
  token = localStorage.getItem('token');
  // removeTodoModal = false;
  todo: ITodo = null;

  constructor(
    private route: ActivatedRoute,
    private router: Router,
    private todoService: TodoService,
    private location: Location,
    private modalService: NgbModal
  ) { }

  ngOnInit(): void {
    this.getTodo();
  }

  getTodo(): void {
    const id = this.route.snapshot.paramMap.get('id');
    this.todoService.getTodo(id, this.token).subscribe(data => this.todo = data);
  }

  // back to previous page
  backPreviousPage(): void {
    this.location.back();
  }

  // show the remove modal
  // and show the remove todo modal
  showRemoveTodoModal(todo: ITodo): void {
    const modalRef = this.modalService.open(ModalRemoveComponent, { centered: true });
    // grab the remove todo data and pass to the modal
    modalRef.componentInstance.currentRemoveTodo = todo;
    // modal response the result which is the remove id: currentRemoveTodo['_id']
    modalRef.result.then((result: string) => {
      if (result !== 'Close') {
        this.removeTodo(result);
      }
    }, (reason) => {
      console.log(reason);
    });
  }

  // remove todo
  removeTodo(id: string): any {
    this.todoService.deleteTodo(id, this.token).subscribe(data => {
      this.todoService.getTodoInfos(this.token);
      // research
      // gettodo
      console.log(data);
    }, error => {
      console.log(error);
    });
    this.router.navigate(['/todos']);
  }

  getCssClass(cat): void {
    return cat.replace(/\s*/g, '').toLowerCase();
  }
}
