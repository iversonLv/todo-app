import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { NgbModal } from '@ng-bootstrap/ng-bootstrap';

// get service
import { TodoService } from '../../services/todo.service';
import { CategoryService } from '../../services/category.service';
import { AuthService } from '../../services/auth.service';

// get model
import { ITodo } from '../../models/todo';
import { ICategory } from '../../models/category';
import { ITodoInfo } from '../../models/todo-info';
import { IUser } from '../../models/user';

import { Label, SingleDataSet } from 'ng2-charts';

import { ModalRemoveComponent } from '../../components/modal-remove/modal-remove.component';

@Component({
  selector: 'app-todo',
  templateUrl: './todo.component.html',
  styleUrls: ['./todo.component.scss'],
})
export class TodoComponent implements OnInit {
  token = localStorage.getItem('token');
  categories: ICategory[] = null;
  todos: ITodo[] = null;
  todoInfos: ITodoInfo = null;

  // tab state
  tab = '';

  // for pie chart data
  categoriesData: SingleDataSet = [];
  categoriesLabel: Label[] = [];

  // totalCount: number = null;
  // totalComplete: number = null;
  searchTodoTotal: number = null;

  // calendar data
  calendarEvent: [] = [];

  searchTerm = '';

  // removeTodoModal = false;
  // removeAllTodoModal = false;
  showSearchText = false;

  removeTodoId: string = null;
  editTodoId: string = null;

  // removeAllTodos = false;
  // currentRemoveTodo: ITodo = null;

  // logined user
  me: IUser = null;

  constructor(
    private todoService: TodoService,
    private categoryService: CategoryService,
    private authService: AuthService,
    private router: Router,
    private modalService: NgbModal
  ) {
    this.getTodos();
    this.getloginedUser();
    this.getCategories();
  }

  ngOnInit(): void {
    this.getTodos();
    this.getloginedUser();
    this.getCategories();
  }

  // get all categories
  getCategories(): any {
    // this.categoryService.getCategories().subscribe(data => {
    //   this.categories = data;
    //   }
    // );
    this.categories = this.categoryService.getCategories();
  }

  // get todos infos
  //   {
  //     "total": 1,
  //     "totalComplete": 1,
  //     "totalInComplete": 0,
  //     "totalWorkCat": 0,
  //     "totalDailylifeCat": 1,
  //     "totalEntertaimentCat": 0
  // }
  // getTodoInfos(): void {
  //   this.todoService.getTodoInfos(this.token).subscribe(data => {
  //     this.totalComplete = data.totalComplete;
  //     this.totalCount = data.total;
  //   });
  // }


  // get todos
  getTodos(params?): any {
    this.todoService.getTodos(this.token, params).subscribe(data => {
      // validate whether params has isComplete
      if (params?.isComplete !== undefined) {
        this.tab = params.isComplete;
      }

      this.todos = data.todos;
      this.searchTodoTotal = data.totalCount;
      // every time any update/delete the todos, will also request todo info api as well to calculate
      this.todoService.todoInfos.subscribe(todoInfos => this.todoInfos = todoInfos);
      this.getCategories();

      // grap piechart label
      this.categoriesLabel = this.categories.map(item => item.value);

      // grap piechart data
      this.categoriesData = [];
      for (const i of this.categoriesLabel) {
        this.categoriesData.push(data.todos.filter(item => item.category === i).length);
      }

      // calendar data format

      this.calendarEvent = this.calendarEventFormat(data.todos);

    }, error => {
      console.log(error);
    });
  }

  calendarEventFormat(events): any {
    return events.map(event => {
          const title = event.title;
          const id = event._id;
          const start = event.start;
          const end = event.end;
          let borderColor;
          let backgroundColor;
          let textColor;
          const classNames = [];
          // add isComplete class to the calendar event that if isComplete could line-through the event
          if (event.isComplete) {
            classNames.push('isComplete');
          }
          if (event.category === 'Daily life') {
            textColor = 'rgb(103 194 58)';
            borderColor = 'rgb(103 194 58 / 10%)';
            backgroundColor = 'rgb(103 194 58 / 20%)';
            classNames.push(event.category.replace(/\s*/g, '').toLowerCase());
          } else if (event.category === 'Work') {
            textColor = 'rgb(245 108 108)';
            borderColor = 'rgb(245 108 108 / 10%)';
            backgroundColor = 'rgb(245 108 108 / 20%)';
            classNames.push(event.category.replace(/\s*/g, '').toLowerCase());
          } else {
            textColor = 'rgb(230 162 60)';
            borderColor = 'rgb(230 162 60 / 10%)';
            backgroundColor = 'rgb(230 162 60 / 20%)';
            classNames.push(event.category.replace(/\s*/g, '').toLowerCase());
          }
          return { title, id, textColor, start, end, borderColor, backgroundColor, classNames };
    });
  }

  // get remove todo id from todo list component emit event
  // and show the remove todo modal
  removeTodoEmit(id: string): any {
    return this.todoService.getTodo(id, this.token).subscribe(data => {
      // grab the remove todo data and pass to the modal
      const modalRef  =  this.modalService.open(ModalRemoveComponent, { centered: true });
      modalRef.componentInstance.currentRemoveTodo = data;
      // modal response the result which is the remove id: currentRemoveTodo['_id']
      modalRef.result.then((result: string) => {
        this.removeTodo(result);
      }, (reason) => {
        console.log(reason);
      });
    }, error => {
      console.log(error);
    });
  }

  // remove todo
  removeTodo(id: string): any {
    this.todoService.deleteTodo(id, this.token).subscribe(data => {
      this.todoService.getTodoInfos(this.token);
      this.reSearch();
    }, error => {
      console.log(error);
    });
  }

  editTitleOrToggleCompleteEmit([todo, e]): any {
    let data: {} = {};
    if (e.target.type === 'text') {
      data = {...todo, title: e.target.value};
    } else {
      data = {...todo, isComplete: e.target.checked};
    }
    this.todoService.getTodoInfos(this.token);
    this.todoService.updateTodo(todo._id, data, this.token).subscribe(d => {
      this.getTodos();
      this.reSearch();
    }, error => {
      console.log(error);
    });
  }

  // search function
  searchTodo(): any {
    if (this.searchTerm.trim() !== '') {
      this.getTodos({q: this.searchTerm.trim()});
    }
  }

  // reset search
  reSearch(): void {
    this.showSearchText = false;
    this.searchTerm = '';
    this.getTodos({q: this.searchTerm.trim()});
  }

  // search input keycode function
  searchInputKeyUp(e): void {
    // if click backspace and search input is empty, should reset the todo list
    if ((e.key === 'Backspace' && this.searchTerm.trim() === '') || e.key === 'Escape') {
      this.reSearch();
    }
    // if click enter should do search function
    if (e.key === 'Enter') {
      this.showSearchText = true;
      this.searchTodo();
    }
  }

  // filter uncomplete todos
  inCompleteLength(todos): void {
    if (todos) {
      return todos.filter(item => item.isComplete !== true).length;
    }
  }

  // complete all
  completeAll(): void {
    this.todoService.completeAllTodos(this.token).subscribe(d => {
      console.log(d);
      this.getTodos();
      this.reSearch();
      this.todoService.getTodoInfos(this.token);
    }, error => {
      console.log(error);
    });
  }

  // show clear all todo modal
  showClearAllTodosModal(): void {
    const modalRef  =  this.modalService.open(ModalRemoveComponent, { centered: true });
    modalRef.componentInstance.removeAllTodos = true;
    modalRef.result.then((result: string) => {
      if (result === 'remove all todos') {
        this.deleteAllTodos();
      }
    }, (reason) => {
      console.log(reason);
    });
  }

  // delete all
  deleteAllTodos(): void {
    this.todoService.deleteAllTodos(this.token).subscribe(d => {
      console.log('Delete all todos');
      this.todoService.getTodoInfos(this.token);
      this.reSearch();
    }, error => {
      console.log(error);
    });
  }

  // get current login user
  getloginedUser(): void {
    this.authService.meInfo.subscribe(me => this.me = me);
    this.authService.getProfile(this.token);
  }

  // logout
  logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }
}
