export interface ITodo {
  _id?: string;
  title: string;
  category: string;
  isComplete?: boolean;
  start: Date;
  end: Date;
}
