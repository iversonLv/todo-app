export interface ITodoInfo {
  total: number;
  totalComplete: number;
  totalInComplete: number;
  totalWorkCat: number;
  totalDailylifeCat: number;
  totalEntertaimentCat: number;
}
