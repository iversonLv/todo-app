import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// guard
import { AuthGuard } from './guards/auth.guard';
import { AnonymousGuard } from './guards/anonymous.guard';

// pages components
import { TodoComponent } from './pages/todo/todo.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { TodoDetailComponent } from './pages/todo-detail/todo-detail.component';
import { TodoEditComponent } from './pages/todo-edit/todo-edit.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { AdminDashboardComponent } from './pages/admin-dashboard/admin-dashboard.component';
import { WrapperLoginedComponent } from './components/wrapper-logined/wrapper-logined.component';


const routes: Routes = [
  {
    path: '', redirectTo: '/login', pathMatch: 'full',
  },
  {
    path: 'todos', component: WrapperLoginedComponent, // then we could use a same wrapper
    canActivateChild: [AuthGuard],
    data: {
      roles: ['user']
    },
    children: [
      {
        path: '', component: TodoComponent,
        data: {
          roles: ['user']
        },
      },
      {
        path: ':id', component: TodoDetailComponent,
        data: {
          roles: ['user']
        },
      },
      {
        path: 'edit/:id', component: TodoEditComponent,
        data: {
          roles: ['user']
        },
      }
    ]
  },
  {
    path: 'profile', component: ProfileComponent,
    canActivate: [AuthGuard],
    data: {
      roles: ['user']
    }
  },
  {
    path: 'dashboard', component: AdminDashboardComponent,
    canActivate: [AuthGuard],
    data: {
      roles: ['admin']
    }
  },
  {
    path: 'login', component: LoginComponent,
    canActivate: [AnonymousGuard]
  },
  {
    path: 'register', component: RegisterComponent,
    canActivate: [AnonymousGuard]
  },
  {
    path: '**', component: PageNotFoundComponent
  }
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
