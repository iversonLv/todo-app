import { BrowserModule } from '@angular/platform-browser';
import { BrowserAnimationsModule } from '@angular/platform-browser/animations';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';
// ng bootstrap design
import { NgbModule, NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
// routering module
import { AppRoutingModule } from './app-routing.module';
// Form module for form builder
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
// guard
import { AuthGuard } from './guards/auth.guard';

// ng2 chart module
import { ChartsModule } from 'ng2-charts';

// calendar
import { FullCalendarModule } from '@fullcalendar/angular'; // the main connector. must go first
import dayGridPlugin from '@fullcalendar/daygrid'; // a plugin
import interactionPlugin from '@fullcalendar/interaction'; // a plugin

import { OwlDateTimeModule, OwlNativeDateTimeModule, OWL_DATE_TIME_FORMATS, OWL_DATE_TIME_LOCALE } from 'ng-pick-datetime';

import { AppComponent } from './app.component';
import { TodoComponent } from './pages/todo/todo.component';
import { TodoListComponent } from './components/todo-list/todo-list.component';
import { PageNotFoundComponent } from './pages/page-not-found/page-not-found.component';
import { TodoFormComponent } from './components/todo-form/todo-form.component';
import { TodoChartComponent } from './components/todo-chart/todo-chart.component';
import { TodoCalendarComponent } from './components/todo-calendar/todo-calendar.component';
import { TodoDetailComponent } from './pages/todo-detail/todo-detail.component';
import { ModalRemoveComponent } from './components/modal-remove/modal-remove.component';
import { TodoEditComponent } from './pages/todo-edit/todo-edit.component';
import { LoginComponent } from './pages/login/login.component';
import { RegisterComponent } from './pages/register/register.component';
import { ProfileComponent } from './pages/profile/profile.component';
import { AvatarComponent } from './components/avatar/avatar.component';
import { AdminDashboardComponent } from './pages/admin-dashboard/admin-dashboard.component';
import { HeaderComponent } from './components/header/header.component';
import { HeaderGeneralComponent } from './components/header-general/header-general.component';
import { WrapperLoginedComponent } from './components/wrapper-logined/wrapper-logined.component';
import { AutoFocusDirective } from './shared/auto-focus/auto-focus.directive';
import { AvatarGroupComponent } from './components/avatar-group/avatar-group.component';
import { TotalInfoChartGroupComponent } from './components/total-info-chart-group/total-info-chart-group.component';
import { CardComponent } from './components/card/card.component';

export const MY_NATIVE_FORMATS = {
  fullPickerInput: {year: 'numeric', month: 'short', day: 'numeric', hour: 'numeric', minute: 'numeric'},
  datePickerInput: {year: 'numeric', month: 'numeric', day: 'numeric'},
  timePickerInput: {hour: 'numeric', minute: 'numeric'},
  monthYearLabel: {year: 'numeric', month: 'short'},
  dateA11yLabel: {year: 'numeric', month: 'long', day: 'numeric'},
  monthYearA11yLabel: {year: 'numeric', month: 'long'},
};

FullCalendarModule.registerPlugins([ // register FullCalendar plugins
  dayGridPlugin,
  interactionPlugin
]);

@NgModule({
  declarations: [
    AppComponent,
    TodoComponent,
    TodoListComponent,
    PageNotFoundComponent,
    TodoFormComponent,
    TodoChartComponent,
    TodoCalendarComponent,
    TodoDetailComponent,
    ModalRemoveComponent,
    TodoEditComponent,
    LoginComponent,
    RegisterComponent,
    ProfileComponent,
    AvatarComponent,
    AdminDashboardComponent,
    HeaderComponent,
    HeaderGeneralComponent,
    WrapperLoginedComponent,
    AutoFocusDirective,
    AvatarGroupComponent,
    TotalInfoChartGroupComponent,
    CardComponent
  ],
  imports: [
    BrowserModule,
    BrowserAnimationsModule,
    AppRoutingModule,
    HttpClientModule,
    FormsModule,
    ReactiveFormsModule,
    NgbModule,
    ChartsModule,
    FullCalendarModule,
    OwlDateTimeModule,
    OwlNativeDateTimeModule,
  ],
  providers: [
    AuthGuard,
    NgbActiveModal,
    { provide: OWL_DATE_TIME_FORMATS, useValue: MY_NATIVE_FORMATS },
  ],
  bootstrap: [AppComponent],
  entryComponents: [ModalRemoveComponent]
})
export class AppModule { }
