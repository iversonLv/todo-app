import { TestBed, async } from '@angular/core/testing';
import { AutoFocusDirective } from './auto-focus.directive';
import { ElementRef } from '@angular/core';

describe('AutoFocusDirective', () => {
  beforeEach(async(() => {
    TestBed.configureTestingModule({
      imports: [

      ],
      declarations: [
        AutoFocusDirective
      ],
      providers: [
        ElementRef
      ]
    }).compileComponents();
  }));

  it('should create an instance', () => {
    const fixture = TestBed.createComponent(AutoFocusDirective);
    const app = fixture.componentInstance;
    expect(app).toBeTruthy();
  });
});
