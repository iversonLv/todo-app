import { FormGroup } from '@angular/forms';

  // confirmpassword must match confirm password
export const MustMatch = (controlName: string, matchControlName: string): any => {
  return (formGroup: FormGroup) => {
    const control = formGroup.controls[controlName]; // password
    const matchingControl = formGroup.controls[matchControlName]; // confirmpassword
    if (matchingControl.errors && !matchingControl.errors.mushMatch) {
      return;
    }
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true });
    } else {
      matchingControl.setErrors(null);
    }
  };
};
