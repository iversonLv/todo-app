import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';

import { AuthService } from '../../services/auth.service';
import { TodoService } from '../../services/todo.service';

import { IUser } from '../../models/user';

import { JwtHelperService } from '@auth0/angular-jwt';
import { BehaviorSubject } from 'rxjs';
import { ITodoInfo } from 'src/app/models/todo-info';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.scss']
})
export class HeaderComponent implements OnInit {
  token = localStorage.getItem('token');
  todoInfos: ITodoInfo = null;

  // logined user
  me: IUser = null;

  isAdmin = false;

  constructor(
    public todoService: TodoService,
    public authService: AuthService,
    private router: Router,
  ) {

  }

  ngOnInit(): void {
    // only login then get login user data
    if (this.isLoggined()) {
      this.todoService.todoInfos.subscribe(todoInfos => this.todoInfos = todoInfos);
      this.todoService.getTodoInfos(this.token);
      this.getloginedUser();

      const helper = new JwtHelperService(); // for decode token
      const decodedToken =  helper.decodeToken(this.token); // for decode token
      if (decodedToken.roles[0] === 'admin') {
        this.isAdmin = true;
      }
    }
  }

  // public getTodoInfos(): any {
  //   this.todoService.getTodoInfos(this.token).subscribe(data => {
  //     this.todoService.todoInfos.next(data);
  //   }, error => {
  //     console.log(error);
  //   });
  // }

  // get current login user
  getloginedUser(): void {
    this.authService.meInfo.subscribe(me => this.me = me);
    this.authService.getProfile(this.token);
  }

  // logout
  logout(): void {
    this.authService.logout();
    this.router.navigate(['/login']);
  }

  isLoggined(): boolean {
    return this.authService.isLoggined();
  }

}
