import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-total-info-chart-group',
  templateUrl: './total-info-chart-group.component.html',
  styleUrls: ['./total-info-chart-group.component.scss']
})
export class TotalInfoChartGroupComponent implements OnInit {
  @Input() data = null;
  @Input() chartType = 'isBarChartGroup';

  constructor() { }

  ngOnInit(): void {
  }

}
