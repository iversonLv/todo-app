import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { TotalInfoChartGroupComponent } from './total-info-chart-group.component';

describe('TotalInfoChartGroupComponent', () => {
  let component: TotalInfoChartGroupComponent;
  let fixture: ComponentFixture<TotalInfoChartGroupComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ TotalInfoChartGroupComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(TotalInfoChartGroupComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
