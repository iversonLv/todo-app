import { Component, OnInit, Input, } from '@angular/core';
import { ChartType, ChartOptions } from 'chart.js';
import { Label, SingleDataSet } from 'ng2-charts';

@Component({
  selector: 'app-todo-chart',
  templateUrl: './todo-chart.component.html',
  styleUrls: ['./todo-chart.component.scss']
})
export class TodoChartComponent implements OnInit {
  @Input() categoriesLabel: Label[] = null;
  @Input() categoriesData: SingleDataSet = [];

  // chart
  // Pie
  public pieChartOptions: ChartOptions = {
    responsive: true,
    legend: {
      position: 'top',
    },
    plugins: {
      datalabels: {
        formatter: (value, ctx) => {
          const label = ctx.chart.data.labels[ctx.dataIndex];
          return label;
        },
      },
    }
  };
  // public pieChartLabels: Label[] = ['a', 'b', 'c'];
  // tslint:disable-next-line:max-line-length
  // public pieChartData: number[] = [1, 2, 3];
  public pieChartType: ChartType = 'pie';
  public pieChartLegend = true;
  public pieChartColors = [
    {
      backgroundColor: ['rgb(103, 194, 58)', 'rgb(245,108,108)', 'rgb(230,162,60)'], // Daily life, Work, Entertaiment
    },
  ];

  constructor() {
  }

  ngOnInit(): void {
  }

  // events
  public chartClicked({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

  public chartHovered({ event, active }: { event: MouseEvent, active: {}[] }): void {
    console.log(event, active);
  }

}
