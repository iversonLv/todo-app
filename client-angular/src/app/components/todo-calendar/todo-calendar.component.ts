import { Component, OnInit, Input, ViewChild } from '@angular/core';
import { Router } from '@angular/router';
import { ITodo } from '../../models/todo';

// calendar
import { CalendarOptions } from '@fullcalendar/angular'; // useful for typechecking

@Component({
  selector: 'app-todo-calendar',
  templateUrl: './todo-calendar.component.html',
  styleUrls: ['./todo-calendar.component.scss']
})
export class TodoCalendarComponent implements OnInit {
  @Input() todos: ITodo = null;

  constructor(
    public router: Router,
  ) {
  }

  ngOnInit(): void {
    // bind this then at the eventClick the this could point to this component then could use router
    this.eventClick = this.eventClick.bind(this);
  }
  // events: [
  //   { title: 'event 1', date: '2019-04-01' },
  //   { title: 'event 2', date: '2019-04-02' }
  // ]

//   0: {title: "All Day Event", start: "2020-09-01"}
// 1: {title: "Long Event", start: "2020-09-07", end: "2020-09-10"}
// 2: {groupId: "999", title: "Repeating Event", start: "2020-09-09T16:00:00+00:00"}
// 3: {groupId: "999", title: "Repeating Event", start: "2020-09-16T16:00:00+00:00"}
// 4: {title: "Conference", start: "2020-08-31", end: "2020-09-02"}
// 5: {title: "Meeting", start: "2020-09-01T10:30:00+00:00", end: "2020-09-01T12:30:00+00:00"}
// 6: {title: "Lunch", start: "2020-09-01T12:00:00+00:00"}
// 7: {title: "Birthday Party", start: "2020-09-02T07:00:00+00:00"}
// 8: {url: "http://google.com/", title: "Click for Google", start: "2020-09-28"}
// 9: {title: "Meeting", start: "2020-09-01T14:30:00+00:00"}
// 10: {title: "Happy Hour", start: "2020-09-01T17:30:00+00:00"}
// 11: {title: "Dinner", start: "2020-09-01T20:00:00+00:00"}

  eventClick(info): any {
    const todoId = info.event.id;
    info.jsEvent.preventDefault();
    // original this point to the calendar component, so we need bind(this)
    this.router.navigate(['/todos/', todoId]);
  }

  eventMouseEnter(info): void {
    info.el.style.cursor = 'pointer';
  }

}
