import { Component, OnInit, Output, EventEmitter, Input, ElementRef, ViewChild } from '@angular/core';

import { ITodo } from '../../models/todo';

@Component({
  selector: 'app-todo-list',
  templateUrl: './todo-list.component.html',
  styleUrls: ['./todo-list.component.scss']
})
export class TodoListComponent implements OnInit {
  @Input() todos: ITodo[] = null;

  @Output() removeTodoEmit: EventEmitter<string> = new EventEmitter();
  @Output() editTitleOrToggleCompleteEmit: EventEmitter<any> = new EventEmitter();

  @ViewChild('editTitleInputElementRef') editTitleInputElementRef: ElementRef;

  currentItem: ITodo = null;

  constructor(
  ) { }

  ngOnInit(): void {
  }

  editTitle(item: ITodo): void {
    this.currentItem = item;
    // dbclick will auto focus the input
    setTimeout(() => {
      if (this.currentItem !== null) {
        this.editTitleInputElementRef.nativeElement.focus();
      }
    }, 300);
  }

  getCssClass(cat): void {
    return cat.replace(/\s*/g, '').toLowerCase();
  }

  onBlurUpdate([item, e]): void {
    const oldValue = item.title;
    const newValue = e.target.value;
    this.update([item, e], oldValue, newValue);
  }

  // press enter to update
  onPressUpdate([item, e]): void {
    const oldValue = item.title;
    const newValue = e.target.value;
    if (e.key === 'Enter') {
      this.update([item, e], oldValue, newValue);
    }
  }

  // general update function
  update([item, e], oldValue, newValue): void {
    this.currentItem = null;
    // if input is new value compare with old title will update or don't call api
    if (newValue !== oldValue) {
      this.editTitleOrToggleCompleteEmit.emit([item, e]);
    }
  }

}

