import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import {  NgbActiveModal } from '@ng-bootstrap/ng-bootstrap';
import { ITodo } from '../../models/todo';

@Component({
  selector: 'app-modal-remove',
  templateUrl: './modal-remove.component.html',
  styleUrls: ['./modal-remove.component.scss']
})
export class ModalRemoveComponent implements OnInit {
  @Input() currentRemoveTodo: ITodo = null;
  @Input() removeAllTodos = false;

  constructor(
    public removeModal: NgbActiveModal
  ) { }

  ngOnInit(): void {
  }

}
