import { Component, Input, OnInit } from '@angular/core';

@Component({
  selector: 'app-avatar-group',
  templateUrl: './avatar-group.component.html',
  styleUrls: ['./avatar-group.component.scss']
})
export class AvatarGroupComponent implements OnInit {
  @Input() data = null;
  @Input() max = null;
  constructor() { }

  ngOnInit(): void {
  }

}
