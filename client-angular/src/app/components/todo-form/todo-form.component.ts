import { Component, OnInit, Output, EventEmitter, Input } from '@angular/core';
import { FormBuilder, Validators, FormGroup } from '@angular/forms';

import { TodoService } from '../../services/todo.service';

import { ITodo } from '../../models/todo';
import { ICategory } from '../../models/category';
import { BehaviorSubject } from 'rxjs';

@Component({
  selector: 'app-todo-form',
  templateUrl: './todo-form.component.html',
  styleUrls: ['./todo-form.component.scss']
})
export class TodoFormComponent implements OnInit {
  @Output() getTodoEmit: EventEmitter<boolean> = new EventEmitter();
  @Input() categories: ICategory[] = null;
  @Input() todos: ITodo[] = null;

  token = localStorage.getItem('token');

  // loading, errorMessage when action form
  loading = false;
  errorMessage: any = null;

  createTodoForm: FormGroup;

  constructor(
    private formBuilder: FormBuilder,
    private todoService: TodoService,
  ) {

    this.createTodoForm = this.formBuilder.group({
      title: ['', Validators.required],
      category: ['', Validators.required],
      startEndRange: [[new Date(), new Date()], Validators.required]
    });

  }

  get f(): any { return this.createTodoForm.controls; }

  ngOnInit(): void {
  }

  // on submit the create todo form
  createTodo(todo: any): any {
    this.loading = true;
    this.errorMessage = null;

    // grab form necessary data, ES6 feature
    const { title, category, startEndRange } = todo;

    const newTodo: ITodo = {
      title,
      category,
      start: startEndRange[0],
      end: startEndRange[1],
    };

    // POST create new todo
    this.todoService.postTodo(newTodo, this.token).subscribe(data => {
      console.log(data);
      this.loading = false;
      // reset title and category default option, so we don't need reset all to empty
      this.formReset();

      // emit a getTodo function to parent page component to GET all todos again
      this.getTodoEmit.emit();

    }, error => {
      console.log(error);
      this.errorMessage = error;
      this.loading = false;
    });

    // Borcast the getTodoInfos to next value
    this.todoService.getTodoInfos(this.token);
  }

  formReset(): void {
    // after create, reset the form
    this.createTodoForm.setValue({
      title: '',
      category: 'Daily life',
      startEndRange: [new Date(), new Date()]
    });
  }


}
