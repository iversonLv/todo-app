import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { WrapperLoginedComponent } from './wrapper-logined.component';

describe('WrapperLoginedComponent', () => {
  let component: WrapperLoginedComponent;
  let fixture: ComponentFixture<WrapperLoginedComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ WrapperLoginedComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(WrapperLoginedComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
