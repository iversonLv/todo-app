import { Injectable } from '@angular/core';
import { GeneralService } from './general.service';

// model
import { ITodo } from '../models/todo';
import { ITodoInfo } from '../models/todo-info';
import { BehaviorSubject, Observable } from 'rxjs';

@Injectable({
  providedIn: 'root'
})
export class TodoService {
  todoInfos: BehaviorSubject<ITodoInfo> = new BehaviorSubject<ITodoInfo>(null);

  constructor(
    private generalService: GeneralService
  ) { }

  tokenHttpOptions = (token) => {
    return {
      'Content-Type':  'application/json',
      // tslint:disable-next-line:object-literal-key-quotes
      'Authorization': `Bearer ${token}`
    };
  }

  // get todos info length
  // GET api/todos/info
  getTodoInfos(token: string, params?): any {
    return this.generalService.get('todos/infos', params, this.tokenHttpOptions(token)).subscribe(data => {
      return this.todoInfos.next(data);
    }, error => {
      console.log(error);
    });
  }

  // get all todos
  // GET api/todos
  getTodos(token: string, params?): any {
    return this.generalService.get('todos', params, this.tokenHttpOptions(token));
  }

  // get one todo
  // GET api/todos/:id
  getTodo(id: string, token: string): any {
    return this.generalService.get(`todos/${id}`, {}, this.tokenHttpOptions(token));
  }

  // create todo
  // POST api/todos
  postTodo(todo: ITodo, token: string): any {
    return this.generalService.post('todos', todo, this.tokenHttpOptions(token));
  }

  // delete todo
  // DELETE api/todos/:id
  deleteTodo(id: string, token: string): any {
    return this.generalService.delete(`todos/${id}`, this.tokenHttpOptions(token));
  }

  // update/put todo
  // put api/todos/:id
  updateTodo(id: string, data: any, token: string): any {
    return this.generalService.put(`todos/${id}`, data, this.tokenHttpOptions(token));
  }

  // complete all todos
  // post api/todos/completeAllTodos
  completeAllTodos(token: string): any {
    return this.generalService.post(`todos/completeAllTodos`, {}, this.tokenHttpOptions(token));
  }

  // delete all todos
  // post api/todos/deleteAllTodos
  deleteAllTodos(token: string): any {
    return this.generalService.post(`todos/deleteAllTodos`, {}, this.tokenHttpOptions(token));
  }
}
