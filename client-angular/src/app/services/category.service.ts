import { Injectable } from '@angular/core';
import { GeneralService } from './general.service';
import { HttpHeaders } from '@angular/common/http';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  constructor(
    private generalService: GeneralService
  ) { }

  categories = [
    {
      id: '1',
      value: 'Daily life'
    },
    {
      id: '2',
      value: 'Work'
    },
    {
      id: '3',
      value: 'Entertaiment'
    }
  ];
  // get all categories
  // GET api/categories
  getCategories(): any {
    return this.categories;
    // return this.generalService.get('categories', {}, {
    //   'Content-Type':  'application/json',
    // });
  }
}
