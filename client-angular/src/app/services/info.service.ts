import { Injectable } from '@angular/core';
import { GeneralService } from './general.service';

@Injectable({
  providedIn: 'root'
})
export class InfoService {
  tokenHttpOptions = (token: string) => {
    return {
      'Content-Type':  'application/json',
      // tslint:disable-next-line:object-literal-key-quotes
      'Authorization': `Bearer ${token}`
    };
  }

  constructor(
    private gneralService: GeneralService
  ) { }

  // get top ten users
  // Get api/info/topTen
  getTopTen(token: string, params?): any {
    return this.gneralService.get('info/topTen', params, this.tokenHttpOptions(token));
  }

  // get users data
  // Get api/info/users
  getUsers(token): any {
    return this.gneralService.get('info/users', {}, this.tokenHttpOptions(token));
  }
}
