import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { Observable, throwError } from 'rxjs';
import {retry, catchError } from 'rxjs/operators';
import { environment } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class GeneralService {

  private BASE_API = environment.BASE_API;

  constructor(
    private httpClient: HttpClient
  ) { }

  // general http get service function
  get(url, params?, headers?): Observable<any> {
    return this.httpClient.get(this.BASE_API + url, {
      headers: {...headers},
      params
    })
    .pipe(
      catchError(this.errorHandle)
    );
  }

  // general http post service function
  post(url, data, headers): Observable<any> {
    return this.httpClient.post(this.BASE_API + url, data, {
      headers
    })
    .pipe(
      retry(1),
      catchError(this.errorHandle)
    );
  }

  // general http delete service function
  delete(url, headers): Observable<any> {
    return this.httpClient.delete(this.BASE_API + url, {
        headers
      })
      .pipe(
        retry(1),
        catchError(this.errorHandle)
      );
  }

  // general http update/put service function
  put(url, data, headers): Observable<any> {
    return this.httpClient.put(this.BASE_API + url, data, {
      headers,
    })
    .pipe(
      retry(1),
      catchError(this.errorHandle)
    );
  }

  // error handle
  errorHandle(error): any {
    let errorMessage = '';
    if (error.error instanceof ErrorEvent) {
      // get client side error
      errorMessage = error.error.message;
    } else {
      // get server side error
      // errorMessage = `Error Code: ${error.status}\nMessage: ${error.error.message}`;
      errorMessage = error.error.message;
    }
    return throwError(errorMessage);

  }
}
