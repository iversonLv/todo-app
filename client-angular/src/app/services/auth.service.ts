import { Injectable } from '@angular/core';

import { GeneralService } from './general.service';
import { HttpHeaders } from '@angular/common/http';
import { BehaviorSubject } from 'rxjs';
import { IUser } from '../models/user';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  meInfo: BehaviorSubject<IUser> = new BehaviorSubject<IUser>(null);
  httpOptions = {
    headers: new HttpHeaders({
      'Content-Type':  'application/json',
    })
  };

  tokenHttpOptions = (token: string) => {
    return {
      'Content-Type':  'application/json',
      // tslint:disable-next-line:object-literal-key-quotes
      'Authorization': `Bearer ${token}`
    };
  }

  constructor(
    private generalService: GeneralService
  ) { }

  // login
  // POST api/auth/login
  loginUser(user): any {
    return this.generalService.post(`auth/login`, user, this.httpOptions);
  }

  // register
  // POST api/auth/signup
  createUser(user): any {
    return this.generalService.post(`auth/signup`, user, this.httpOptions);
  }

  // get user
  // GET api/auth/me
  getProfile(token): any {
    return this.generalService.get(`auth/me`, {}, this.tokenHttpOptions(token)).subscribe(data => {
      return this.meInfo.next(data.user);
    }, error => {
      console.log(error);
    });
  }

  // update user
  // PUT api/auth/me
  // upload avatar should not set 'Content-Type':  'application/json' as it's use FormData()
  updateMe(user, token): any {
    return this.generalService.put(`auth/me`, user, {
      // tslint:disable-next-line:object-literal-key-quotes
      'Authorization': `Bearer ${token}`
    });
  }

  // check whether there is token, this is check for auth guard canActivate
  isLoggined(): any {
    return !!localStorage.getItem('token');
  }

  // logout
  logout(): void {
    localStorage.removeItem('token');
  }
}
