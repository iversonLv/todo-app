import { Injectable } from '@angular/core';
import { CanActivateChild, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree, CanActivate } from '@angular/router';
import { Observable } from 'rxjs';

import { Router } from '@angular/router';

import { JwtHelperService } from '@auth0/angular-jwt';



@Injectable({
  providedIn: 'root'
})
export class AuthGuard implements CanActivateChild, CanActivate {
  constructor(
    private router: Router,
    ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return this.authGuardGeneral(next, state);
  }

  canActivateChild(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      return this.authGuardGeneral(next, state);
  }

  authGuardGeneral(next, state): any {
    const helper = new JwtHelperService();
    const token = localStorage.getItem('token');
    // check whether there is token, logout will remove localstorage token
    // check whether undefined token login with wrong user/password will set a undefinded token in localstorage
    if (token !== 'undefined' && token) {
      const decodedToken = helper.decodeToken(token);
      // validate decode token
      if (decodedToken !== null) {
        // validate decode roles compare with route roles
        if (next.data.roles.includes(decodedToken.roles[0])) {
          return true;
        } else {
          this.router.navigate(['/login']);
          return false;
        }
      } else {
          this.router.navigate(['/login']);
          return false;
      }
    } else {
      this.router.navigate(['/login']);
      return false;
    }
  }

}
