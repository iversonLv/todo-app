import { Injectable } from '@angular/core';
import { CanActivate, ActivatedRouteSnapshot, RouterStateSnapshot, UrlTree } from '@angular/router';
import { Observable } from 'rxjs';

import { Router } from '@angular/router';

import { JwtHelperService } from '@auth0/angular-jwt';

@Injectable({
  providedIn: 'root'
})
export class AnonymousGuard implements CanActivate {
  constructor(
    private router: Router,
    ) {}
  canActivate(
    next: ActivatedRouteSnapshot,
    state: RouterStateSnapshot): Observable<boolean | UrlTree> | Promise<boolean | UrlTree> | boolean | UrlTree {
      const helper = new JwtHelperService();
      const token = localStorage.getItem('token');
      // if isLoggined will go to todos page if manually type /login or /register in URL
      if (token !== 'undefined' && token) {
        // TODO: now the angular-jwt does not support invalid token to be null
        const decodedToken = helper.decodeToken(token);
        // validate decode token
        if (decodedToken !== null) {
          // validate decode token roles
          if (decodedToken.roles[0] === 'user') {
            this.router.navigate(['/todos']);
          } else if (decodedToken.roles[0] === 'admin') {
            this.router.navigate(['/dashboard']);
          } else {
            console.log(4);
            return true;
          }
        } else {
          console.log(1);
          return true;
        }
      } else {
        console.log(2);
        return true;
      }
  }
}
