const { gql } = require('apollo-server')

const typeDefs = gql`
# Scalar
  scalar Date

# Type
  type User {
    _id: ID
    username: String!
    password: String!
    roles: [String]
    todos: [Todo]
    avatar: String
    createdAt: User
    updatedBy: User
    createdOn: Date
    updatedOn: Date
  }

  type Todo {
    _id: ID
    title: String!
    start: Date!
    end: Date!
    category: String!
    isComplete: Boolean!
    createdBy: User
    updatedBy: User
    createdOn: Date
    updatedOn: Date
  }

  type TotalInfo {
    total: Int
    totalComplete: Int
    totalInComplete: Int
    totalWorkCat: Int
    totalDailylifeCat: Int
    totalEntertaimentCat: Int
  }

# input type
  input UpdateMeInput {
    username: String!
    currentpassword: String!
    password: String!
  }

  # {
  #   "title": "try to remove pre 3",
  #   "start": "2018-07-10 20:23",
  #   "end": "2018-07-10 20:23",
  #   "category": "Daily life"
  # }
  input NewTodoInput {
    title: String!
    start: Date!
    end: Date!
    category: String!
  }

  # {
  #   "title": "Do som",
  #   "start": "2018-07-10 20:22",
  #   "end": "2018-07-10 20:23",
  #   "category": "Daily life",
  #   "isComplete": true
  # }
  input UpdateTodoInput {
    title: String!
    start: Date!
    end: Date!
    category: String!
    isComplete: Boolean
  }

# Query
  type Query {
    # userResolvers
    me: User!

    # todoResolvers
    infos: TotalInfo
    # getAlltodos: [Todo]
    getTodo(parmasId: ID): Todo
  }

# Mutation
  type Mutation {
    # userResolvers
    signup(username: String!, password: String!): String!
    adminSignup(username: String!, password: String!): String!
    login(username: String!, password: String!): String!
    updateMe(updateMeInput: UpdateMeInput): String!

    # todoResolvers
    newTodo(newTodoInput: NewTodoInput ): Todo
    updateTodo(parmasId: ID, updateTodoInput: UpdateTodoInput) : Todo
    deleteTodo(parmasId: ID): String!
    # completeAllTodos() :
    # deleteAllTodos() :
  }
`

module.exports = typeDefs