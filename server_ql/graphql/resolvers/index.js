const todoResolver = require('./todo')
const userResolvers = require('./user')

const resolvers = {
  Query: {
    ...todoResolver.Query,
    ...userResolvers.Query,
  },
  Mutation: {
    ...todoResolver.Mutation,
    ...userResolvers.Mutation,
  }
}

module.exports = resolvers