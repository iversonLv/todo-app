const { CheckResultAndHandleErrors, UserInputError } = require('apollo-server')
// auth middlewares
const { checkTokenSetUser } = require('../../auth/middlewares')
// model
const Todo = require('../../models/Todo')
const User = require('../../models/User')

// joi for model schema
const Joi = require('joi')
// validate model schema
const todoSchema = Joi.object({
  title: Joi.string().trim().max(100).required(),
  start: Joi.date().required(),
  end: Joi.date().required(),
  category: Joi.string().required(),
  isComplete: Joi.boolean()
}).options({ abortEarly: false })
// get all todo infos
// {
//   "total": 15,
//   "totalComplete": 0,
//   "totalInComplete": 15,
//   "totalWorkCat": 0,
//   "totalDailylifeCat": 0,
//   "totalEntertaimentCat": 0
// }
const infos = async (_, {}, context) => {
  const id = await checkTokenSetUser(context)._id
  const roles = await checkTokenSetUser(context).roles
  const adminRole = await roles.includes('admin')

  // if admin role will fetch all info data, if user, just fetch the users' data
  const total = await Todo.countDocuments(adminRole ? {} : {createdBy: id});
  const totalComplete = await Todo.countDocuments(adminRole ? {isComplete: true} : {createdBy: id, isComplete: true});
  const totalInComplete = await Todo.countDocuments(adminRole ? {isComplete: false} : {createdBy: id, isComplete: false});
  const totalWorkCat= await Todo.countDocuments(adminRole ? {category: 'Work'} : {createdBy: id, category: 'Work'});
  const totalDailylifeCat= await Todo.countDocuments(adminRole ? {category: 'Daily life'} : {createdBy: id, category: 'Daily life'});
  const totalEntertaimentCat= await Todo.countDocuments(adminRole ? {category: 'Entertaiment'} : {createdBy: id, category: 'Entertaiment'});

  return {
    total,
    totalComplete,
    totalInComplete,
    totalWorkCat,
    totalDailylifeCat,
    totalEntertaimentCat
  }
}

// Get all todos
const getAlltodos = async (_, {}, context) => {

}

// Create new todo
const newTodo = async (_, { newTodoInput: { title, start, end, category } }, context) => {
  const id = checkTokenSetUser(context)._id

  const { value, error } = todoSchema.validate({ title, start, end, category })
  if (error) throw new UserInputError(error)
  const newTodo = new Todo({
    ...value,
    createdOn: (new Date()).toLocaleString(),
    createdBy: id,
    updatedOn: (new Date()).toLocaleString(),
    updatedBy: id
  })
  const todo = await newTodo.save()
  await User.update({ _id: id}, { $push: {todos: todo } })
  return todo
}

// Get Single todos
const getTodo = async (_, { parmasId }, context) => {
  const id = checkTokenSetUser(context)._id
  const todo = await Todo.findOne({ _id: parmasId, createdBy: id }).populate('createdBy', 'username')
  if (!todo) throw new CheckResultAndHandleErrors('No such todo')
  return todo
}

// Update todo
const updateTodo = async (_, { parmasId, updateTodoInput: { title, start, end, category, isComplete }}, context) => {
  const id = checkTokenSetUser(context)._id

  const { value, error } = todoSchema.validate({ title, start, end, category, isComplete })
  if (error) throw new UserInputError(error)
  const updateTodo = {
    ...value,
    updatedOn: (new Date()).toLocaleString(), // toLocalString() 将 greenwich timezone change to local timezone
    updatedBy: id,
  }

  const todo = await Todo.findOneAndUpdate({ _id: parmasId, createdBy: id }, { $set: updateTodo }, { new: true }).populate('updatedBy', 'username')
  if(!todo) throw new CheckResultAndHandleErrors('No such todo')
  return todo
}

// Delete todo
const deleteTodo = async (_, { parmasId }, context) => {
  const id = checkTokenSetUser(context)._id

  const todo = await Todo.findOne({ _id: parmasId, createdBy: id })
  if(!todo) throw new CheckResultAndHandleErrors('No such todo')
  // after delete todo, need pull the todo from User todos list
  await User.update({ _id: id }, { $pull: {todos: {$in: parmasId } } })
  await todo.remove()
  return `Todo '${todo.title}' has been delete`

}

// Complete all todos
const completeAllTodos = async (req, res) => {}

// Delete all todos
const deleteAllTodos = async (req, res) => {}

module.exports = {
  Query: {
    infos,
    // getAlltodos,
    getTodo,
  },
  Mutation: {
    newTodo,
    updateTodo,
    deleteTodo,
    // completeAllTodos,
    // deleteAllTodos,
  }
}