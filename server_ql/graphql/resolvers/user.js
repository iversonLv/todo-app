// auth middlewares
const { checkTokenSetUser } = require('../../auth/middlewares')

// upload option
const { upload } = require('../../utils/upload')


// graphql userinput error
const { UserInputError, AuthenticationError, CheckResultAndHandleErrors } = require('apollo-server')
const User = require('../../models/User')
const Todo = require('../../models/Todo')

// Joi for mdel schema
const Joi = require('joi')

const bcrypt = require('bcryptjs')
const { registerUser, loginUser, validateUsername, bcryptPassword } = require('../../utils/auth')

// validate model schema
const userSchema = Joi.object({
  username: Joi.string().regex(/(^[a-zA-Z0-9_]*$)/).min(2).max(30).required(),
  password: Joi.string().trim().min(10).required()
}).options({ abortEarly: false })

// update user model schema
const updateUserSchema = Joi.object({
  username: Joi.string().regex(/(^[a-zA-Z0-9_]*$)/).min(2).max(30).required(),
  password: Joi.string().trim().min(10).required(),
  //avatar: Joi.any(), // avatar now is optional and any value, undefinded or not
  currentpassword: Joi.string().trim().min(10).required()
}).options({ abortEarly: false })

// user signup
const signup = async (_, { username, password } ) => {
  const {value, error} = await userSchema.validate({ username, password })
  if (error) {
    throw new UserInputError(error)
  }
  const token = await registerUser(value, ['user'])
  return token
}

// adminsignup
const adminSignup = async (_, { username, password }) => {
  const { value, error } = await userSchema.validate({ username, password })
  if (error) throw new UserInputError(error)
  const token = await registerUser(value, ['admin'])
  return token
}

// login
const login = async (_, { username, password }) => {
  // here joi v17 is diffrent old version?
  // await userSchema.valudate(...)
  const {value, error} = await userSchema.validate({ username, password })
  if (error) {
    throw new UserInputError(error)
  }
  const token = await loginUser(value)
  return token
}

// get current user
// need authentication check
// check Token, check login
const me = async (_, {}, context) => {
  const id = checkTokenSetUser(context)._id
  const user = await User.findById(
    { _id: id }, 
    // 'username roles todos avatar' // Graphql does not use here, query will take charge of it
  )
  .populate({
    'model': Todo, // graphql need this
    'path': 'todos', // populate todos
    'select': 'title createdOn', // only display title and createdOn
    'options': {
      sort: { createdOn: -1 } //sort createdOn from latest to old
    }
  })
  if (user === null) throw new CheckResultAndHandleErrors('No such user')
  return user
}

// update current user
// need authentication check
// check Token, check login
const updateMe = async (_, { updateMeInput: { username, password, currentpassword} }, context) => {
  
  // upload.single('avatar')
  const id = checkTokenSetUser(context)._id
  // first validate the input with joi
  const { value, error } = updateUserSchema.validate({ username, password, currentpassword})
  if (error) throw new UserInputError(error)

  // check whether the username is unique or not
  const hasUser = await validateUsername(value.username)
  if (hasUser && hasUser._id.toString() !== id) {
    throw new CheckResultAndHandleErrors('The username has been taken')
  }

  const user = await User.findOne({ _id: id })
  // validate there is no user
  if (user === null ) throw new CheckResultAndHandleErrors('No such user')

  // there is user
  const inputPassword = value.currentpassword.trim()
  const dbPassword = user.password
  const result = await bcrypt.compare(inputPassword, dbPassword)
  if (!result) throw new CheckResultAndHandleErrors('Current password is not correct')
  
  const hashedPassword = await bcryptPassword(value.password)
  const updatedMe = {
    username: value.username,
    password: hashedPassword,
    // avatar: context.req.file !== undefined ? 'http://' + context.req.host + '\/' + context.req.file.path : user.avatar, // here if no file, would use existing user.avatar value
    updatedOn: (new Date()).toLocaleString(), // toLocalString() greenwich timezone change to local timezone
    updatedBy: id,
  }
  await user.update({ $set: updatedMe })
  // update will return { n: 1, nModified: 1, ok: 1 } rather than the update obj
  return 'updated succeed!'
}

module.exports = {
  Query: {
    me,
  },
  Mutation: {
    signup,
    adminSignup,
    login,
    updateMe
  }
}