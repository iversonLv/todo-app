const mongoose = require('mongoose')
const Schema = mongoose.Schema

const UserSchema = new Schema({
  username: { type: String, required: true, unique: true },
  password: { type: String, required: true },
  roles: { type: [String], default: 'user', enum: ['user', 'admin']},
  todos: [{ type: Schema.Types.ObjectId, ref: 'Todo' }],
  avatar: { type: String },
  createdBy: { type: Schema.Types.ObjectId, ref: 'User' },
  updatedBy: { type: Schema.Types.ObjectId, ref: 'User'},
}, { timestamps: { createdAt: 'createdOn', updatedAt: 'updatedOn' } })

module.exports = mongoose.model('User', UserSchema)