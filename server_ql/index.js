require('dotenv').config() // for TOKEN_SECRIT .env file
const mongoose = require('mongoose')
mongoose.Promise = global.Promise

// mongoose connet
const db = mongoose.connection
mongoose.connect('mongodb://localhost/todos', {
  useNewUrlParser: true,
  useCreateIndex: true,
  useFindAndModify: false,
  useUnifiedTopology: true
})
db.on('error', console.error.bind(console, 'connection error'))
db.once('open', () => console.log(`db connect ${process.env.DB_C}`))

const { ApolloServer } = require('apollo-server')
// typeDefs
const typeDefs = require('./graphql/typeDefs')
// resolvers
const resolvers = require('./graphql/resolvers')

const port = process.env.PORT || 5001
const server = new ApolloServer({
  typeDefs,
  resolvers,
  context: ({ req }) => ({ req })
});

// The `listen` method launches a web server.
server.listen({ port }).then(() => {
  console.log(`listening on http://localhost:${port}`)
});