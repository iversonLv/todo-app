const { AuthenticationError } = require('apollo-server')
const jwt = require('jsonwebtoken')

const checkTokenSetUser = (context) => {
  const authHeader = context.req.headers.authorization
  if (authHeader) {
    const token = authHeader.split('Bearer ')[1]
    if (token) {
        return jwt.verify(token, process.env.TOKEN_SECRET, (err, decoded) => {
          if (err) {
            throw new AuthenticationError(err)
          }
          return decoded
        })
    } else {
      throw new AuthenticationError('No token')
    }
  } else {
    throw new AuthenticationError('No-Authorization')
  }
}

module.exports = {
  checkTokenSetUser
}