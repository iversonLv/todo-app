// graphql userinput error
const { AuthenticationError } = require('apollo-server')
// model
const User = require('../models/User')

const bcrypt = require('bcryptjs')
const saltRounds = 10;

// jwt
const jwt = require('jsonwebtoken');

// jwt token function
const createTokenSendResponse = (user) => {
  const loginUser = {
    _id: user.id,
    username: user.username,
    roles: user.roles
  }
  // jwt sign
  return jwt.sign(
    loginUser,
    process.env.TOKEN_SECRET,
    { expiresIn: '7d' }
  )
}

// validate whether there is user
const validateUsername = async (username) => {
  const user = await User.findOne({ username })
  return user
}

// Hashed passwrd
const bcryptPassword = (password) => {
  return bcrypt.hash(password.trim(), saltRounds)
}

const registerUser = async (reqBody, roles) => {
  const { username, password } = reqBody
  const hasUser = await validateUsername(username)
  //check username is unique or not
  if (hasUser) {
    // There is already a user in the db with this username..
    // Resoponse error
    throw new AuthenticationError('The username has been taken')
  }
  // hash password
  // inssert the user to db
  const hasedPassword = await bcryptPassword(password)
  const newUser = new User({
    username: username,
    password: hasedPassword,
    roles: [...roles]
  })
  const user = await newUser.save()
  return createTokenSendResponse(user)
}

const loginUser  = async (reqBody) => {
  const { username, password } = reqBody
  const hasUser = await validateUsername(username)
  // verify whether there is user
  if (!hasUser) {
    throw new AuthenticationError('username wrong, please check')
  }
  const inputPassword = password.trim()
  const dbPassword = hasUser.password
  const result = await bcrypt.compare(inputPassword, dbPassword)
  if (result) {
    return createTokenSendResponse(hasUser)
  } else {
    throw new AuthenticationError('password is not correct')
  }
}

module.exports = {
  registerUser,
  loginUser,
  validateUsername,
  bcryptPassword
}

