const express = require('express')
const router = express.Router()

const TodoController = require('../controller/todo')

// auth middleware
const authMiddleware = require('../auth/middlewares')

// router check auth middleware
router.use(authMiddleware.checkTokenSetUser);

// {{URL}}/api/v1/todos/info
router.route('/infos', authMiddleware.isLogined)
// Get todos info
      .get(TodoController.infos)

// {{URL}}/api/v1/todos
router.route('/', authMiddleware.isLogined)
// Get all todos
      .get(TodoController.getAlltodos)
// Create new todo
      .post(TodoController.newTodo)

// {{URL}}/api/v1/todos/:id
router.route('/:id', authMiddleware.isLogined)
// Get Single todos
      .get(TodoController.getTodo)
// Update todo
      .put(TodoController.updateTodo)
// Delete todo
      .delete(TodoController.deleteTodo)

// {{URL}}/api/v1/todos/completeAllTodos
router.route('/completeAllTodos', authMiddleware.isLogined)
/// Complete all todos
      .post(TodoController.completeAllTodos)

// {{URL}}/api/v1/todos/deleteAll
// This might refactor to soft delete
router.route('/deleteAllTodos', authMiddleware.isLogined)
// Delete all todos
      .post(TodoController.deleteAllTodos)

// router.get('/', authMiddleware.isLogined, async (req, res, next) => {

  // let {pageIndex, pageSize, sortOrder = 'desc', sortColumn = 'createdOn', q='', isComplete} = req.query
  // const seachDefined = (q) => {
  //   return {
  //     $search: q, $caseSensitive: false, $diacriticSensitive: false 
  //   }
  // } 
  // try {
  //   let total, data;
  //   //{user_id: req.user._id} this is force
  //   if (q) {
  //     total = await todos.count({user_id: req.user._id, $text: seachDefined(q)})
  //   } else if (isComplete) {
  //     total = await todos.count({user_id: req.user._id, isComplete: isComplete === 'true'})
  //   } else {
  //     total = await todos.count({user_id: req.user._id})
  //   }

  //   pageIndex = parseInt(pageIndex) < 0 ? 1 : parseInt(pageIndex) || 1 // pageIndex must be 0 or > 0 interger
  //   pageSize = parseInt(pageSize) < 0 ? total : parseInt(pageSize) || total //pageSize range between (5, 50)
  //   pageSize = pageSize > total ? total : pageSize // if pageSize exceed total, display total
  //   sortOrder = sortOrder.toLowerCase() === '' ? 'desc' : sortOrder.toLowerCase()
  //   let newPageIndex = Math.ceil(total / pageSize)
  //   if (newPageIndex < pageIndex) {
  //     pageIndex = newPageIndex
  //   }
  //   // sort base on the column
  //   const sort = {};
  //   sort[sortColumn] = sortOrder === 'desc' ? -1 : 1

  //   // this is also detect at front-end as well
  //   if (q) {
  //     data = await todos.find({user_id: req.user._id, $text: seachDefined(q)}, {
  //       skip: (pageIndex - 1) * pageSize,
  //       limit: pageSize,
  //       sort: sort
  //       // sort: {
  //       //   createdOn : sortOrder === 'desc' ? -1 : 1
  //       //   //sortColumn : sortOrder === 'desc' ? -1 : 1
  //       //   //"createdOn": -1
  //       // }
  //     })
  //   } else if (isComplete) {
  //     data = await todos.find({user_id: req.user._id, isComplete: isComplete === 'true'}, {
  //       skip: (pageIndex - 1) * pageSize,
  //       limit: pageSize,
  //       sort: sort
  //       // sort: {
  //       //   createdOn : sortOrder === 'desc' ? -1 : 1
  //       //   //sortColumn : sortOrder === 'desc' ? -1 : 1
  //       //   //"createdOn": -1
  //       // }
  //     })
  //   } else {
  //     data = await todos.find({user_id: req.user._id}, {
  //         skip: (pageIndex - 1) * pageSize,
  //         limit: pageSize,
  //         sort: sort
  //     })
  //   }
  //   console.log(sortColumn, isComplete)
  
  //   res.status(200).json({
  //     total,
  //     pageIndex,
  //     pageSize,
  //     sortColumn,
  //     sortOrder,
  //     data
  //   })
  // } catch (err) {
  //   console.log(err)
  // }
  
  // Promise.all([
  //   todos.count(),
  //   todos.find({
  //     // Specific different todos for current user, see middlewares isLoggined
  //     user_id: req.user._id
  //   }, {
  //     skip: (pageIndex - 1) * pageSize,
  //     limit: pageSize,
  //     sort: {
  //       sortColumn: sortOrder === 'desc' ? -1 : 1
  //     }
  //   })
  // ])
  // .then(([total, todos]) => {
  //   res.status(200).json({
  //     total,
  //     pageIndex,
  //     pageSize,
  //     sortColumn,
  //     sortOrder,
  //     todos
  //   })
  // }).catch(err => {
  //   console.log(err)
  // })
// })

// search = async (criteria) => {
//   // const filter = {}
//   // if (criteria.title) {
//   //   filter.title = criteria.title
//   // }
//   // if (criteria.category) {
//   //   filter.category = criteria.category
//   // }
//   if (criteria.isComplete) {
//     filter.isComplete = criteria.isComplete
//   }
//   // if (criteria.start) {
//   //   filter.start = criteria.start
//   // }
//   // if (criteria.end) {
//   //   filter.end = criteria.end
//   // }

//   const total = await todos.count(filter)

//   if (criteria.sortBy === 'id') {
//     criteria.sortBy = '_id'
//   }
//   let sortStr = `${criteria.sortOrder.toLowerCase() === 'asc' ? '' : '-'}${criteria.sortBy}`
//   if (criteria.sortBy !== '_id') {
//     sortStr += ' _id'
//   }

// }


module.exports = router