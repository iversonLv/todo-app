const express = rquired('express')
const router = express.Router()
const Joi = require('Joi')

// db connection
const db = require('../db/connection')

// Collection
const categories = db.get('categories')
// index
categories.createIndex('name')

// Data example
// {
//   '_id': 234234,
//   'name': 'Work',
//   'createdOn':
//   'createdBy':
//   'updatedOn':
//   'updatedBy':
//   'todos': [
//     {...Joi.}
//   ]
// }

// Data model schema
const schema = Joi.object().keys({
  value: Joi.string().trim().required(),
  createdOn: Joi.date(),
  createdBy: Joi.string(),
  lastModifiedOn: Joi.date(),
  lastModifiedBy: Joi.string(),
  user_id: Joi.string(),
  toods: Joi.array(),
})