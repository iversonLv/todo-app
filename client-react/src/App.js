import React from 'react';
import styles from './App.scss';

import  { BrowserRouter } from 'react-router-dom'
import { RouterOutlet } from './AppRouting'


function App() {
  return (
      <BrowserRouter>
        <div className={ styles.container }>
            {/* router outlet */}
            <RouterOutlet />
          </div>
        {/* footer */}
        <footer>
          <div className={ styles.footer }>
            <img src="./assets/images/small-logo.png" alt="todos" />
          </div>
        </footer>
      </BrowserRouter>
  );
}

export default App;
