import React from 'react'
import  { Route, Redirect, Switch } from 'react-router-dom'

import WrapperLogined from './components/WrapperLogined/WrapperLogined'
import { PrivateRoute, AnonymouseRoute } from './components/GuardRoute/GuardRoute'

// pages
import Login from './pages/Login/Login'
import Register from './pages/Register/Register'
import Pagenotfound from './pages/PageNotFound/PageNotFound'
import Profile from './pages/Profile/Profile'
import AdminDashboard from './pages/AdminDashboard/AdminDashboard'
import Todo from './pages/Todo/Todo'
import TodoDetail from './pages/TodoDetail/TodoDetail'
import TodoEdit from './pages/TodoEdit/TodoEdit'

const TodoRouterOutlet = () => {
  return (
    <Switch>
      <Route exact path="/todos" component={ Todo } />
      <Route exact path="/todos/:id" component={ TodoDetail }/>
      <Route exact path="/todos/edit/:id" component={ TodoEdit }/>
    </Switch>
  )
}

const RouterOutlet = () => {
  return (
    <Switch>
      <AnonymouseRoute path="/login" component={ Login }/>
      <AnonymouseRoute path="/register" component={ Register }/>

      {/* guard for admin and user*/}     
      <PrivateRoute path="/profile" component={ Profile } roles="['user', 'admin']"/>

      {/* guard for user only */}
      <PrivateRoute path="/todos" component={ WrapperLogined } roles="['user']"/>
      {/* guard for admin only*/}
      <PrivateRoute path="/dashboard" component={ AdminDashboard } roles="['admin']"/>

      <Redirect from='/' to="/login" />
      <Route path="*" component={ Pagenotfound }/>
    </Switch>
  )
}

export {
  RouterOutlet,
  TodoRouterOutlet
}