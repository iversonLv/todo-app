import React from 'react'

import Header from '../Header/Header'

import { TodoRouterOutlet } from '../../AppRouting'

const WrapperLogined = () => {
  return (
    <>
      <Header/>
      <TodoRouterOutlet />
      abc
    </>
  )
}

export default WrapperLogined