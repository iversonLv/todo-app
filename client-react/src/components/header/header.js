import React, { useState, useEffect } from 'react'
import  { NavLink, Link } from 'react-router-dom'
import styles from './Header.scss'
import { decodeToken } from "react-jwt";

import Avatar from '@material-ui/core/Avatar'

import fetchDataServices from '../../services/FetchDataServices'

const Header = () => {
  const [me, setMe] = useState({});
  const [totalCount, setTotalCount] = useState(0);
  // if authen
  useEffect(() => {
    const token = localStorage.getItem('token')
    const decodedToken = decodeToken(token)
    const reqBody = {
      query: `
        query {
          me {
            username
            roles
            avatar
          }
        }
      `
    }
    const reqBodyInfos = {
      query: `
        query {
          infos {
            total
          }
        }
      `
    }
    if (token !== 'undefinded' && token && decodedToken !== null) {
      // fetch top right me info
      fetchDataServices(reqBody, token).then(data => {
        setMe(data.me)
      }, error => {
        console.log(error)
      })

      // fetch top left header total todos
      fetchDataServices(reqBodyInfos, token).then(data => {
        setTotalCount(data.infos.total)
      }, error => {
        console.log(error)
      })
    }
  }, [])


  return (
  // header
  <header>
    <div className={ styles.header }>
      <div className={ styles.headerLeft }>
        <h1>{ totalCount !== null ? '(' + totalCount + ')' : '' }</h1>
        <div className={ styles.headerLinks }>
          <NavLink to="/todos" exact activeClassName={ styles.routerLinkActive}>Home</NavLink>
          <a>About</a>
        </div>
      </div>
      <div className={ styles.headerRight }>
        <div className={ styles.userInfo }>
          <p className={ styles.userName }>
            <span>Welcome!</span>
            <Link to="/profile">
              { me.username }
            </Link>
          </p>
          <p className={ styles.userRole }>Role: { me.roles }</p>
        </div>
        <Avatar alt={ me.username } src={ me.avatar } />
        {/* <app-avatar [routerLink]="['/profile']" [avatar]="me.avatar" className={ styles.avatar">{{me.username.slice(0, 2)}}</app-avatar> */}
        <a className={ `${styles.btn} ${styles.btnDanger}` } click="logout()">
          <svg width="1.5em" height="1.5em" viewBox="0 0 16 16" className={ `${styles.bi} ${styles.biPower}` } fill="currentColor" xmlns="http://www.w3.org/2000/svg">
            <path fillRule="evenodd" d="M5.578 4.437a5 5 0 1 0 4.922.044l.5-.866a6 6 0 1 1-5.908-.053l.486.875z"/>
            <path fillRule="evenodd" d="M7.5 8V1h1v7h-1z"/>
          </svg>
        </a>
      </div>
    </div>
  </header>
  )
}

export default Header