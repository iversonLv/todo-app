import React, { Component } from 'react'
import { Route, withRouter } from 'react-router-dom'
import { decodeToken } from "react-jwt";


export class PrivateRoute extends Component {
  token  = localStorage.getItem('token')

  componentDidMount() {
    const { history, roles } = this.props
    // validate token
    if (this.token !== 'undefined' && this.token) {
      const decodedToken = decodeToken(this.token);
      // validate decode token
      if (decodedToken !== null) {
        // validate decode roles compare with route roles
        if (roles.includes(decodedToken.roles[0])) {
          return true;
        } else {
          history.replace('/login')
          return false
        }
      } else {
        history.replace('/login')
        return false
      }
    } else {
      history.replace('/login')
      return false
    }
  }
  
  render() {
    let { component: Component, path="/", exact=false, strict=false, roles=[] } = this.props
    return (
      <Route
        path={path}
        exact={exact}
        strict={strict}
        roles={roles}
        render={(props) => (<Component {...props} />)}
      />
    )
  }
}

PrivateRoute = withRouter(PrivateRoute)


export class AnonymouseRoute extends Component {
  token  = localStorage.getItem('token')

  componentDidMount() {
    const { history } = this.props
    // validate token
    if (this.token !== 'undefined' && this.token) {
      const decodedToken = decodeToken(this.token);
      // validate decode token
      if (decodedToken !== null) {
        // validate decode token roles
        if (decodedToken.roles[0] === 'user') {
          history.replace('/todos')
        } else if (decodedToken.roles[0] === 'admin') {
          history.replace('/dashboard')
        } else {
          console.log(4);
          return false;
        }
      } else {
        console.log(1);
        return false;
      }
    } else {
      console.log(2);
      return false;
    }
  }
  
  render() {
    let { component: Component, path="/", exact=false, strict=false } = this.props
    return (
      <Route
        path={path}
        exact={exact}
        strict={strict}
        render={(props) => (<Component {...props} />)}
      />
    )
  }
}

AnonymouseRoute = withRouter(AnonymouseRoute)