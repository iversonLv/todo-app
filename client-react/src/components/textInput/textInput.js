import React from 'react'
import styles from './TextInput.scss'

// material ui components
import TextField from '@material-ui/core/TextField';

const TextInput = ({ handler, dirty, hasError, meta }) => (
  <div>
    <div className={ styles.formRow }>
      <label htmlFor={ meta.id } className={ styles.label }>
        { meta.label }:
      </label>
      <TextField id={ meta.id } label={ meta.label } type={ meta.type } { ...handler() }/>
    </div>
    { meta.errors && <div className={ styles.formError }>
      { meta.errors.map((error, index) => {
        let errorKey = Object.keys(error)[0];
        let errorMessage = Object.values(error)[0];
          return (
            <p key={ index }>
                { dirty
                && hasError(errorKey)
                && `${ meta.label } ${ errorMessage }` }
            </p>
          )
        })
      }
    </div>
    }
  </div>
)

export default TextInput