import React from 'react'
import styles from './HeaderGeneral.scss'

const HeaderGeneral = () => {
  return (
  <header>
    <div className={ styles.header }>
      <div className={ styles.headerLeft }>
        <h1>&nbsp;</h1>
      </div>
    </div>
  </header>
  )
}

export default HeaderGeneral