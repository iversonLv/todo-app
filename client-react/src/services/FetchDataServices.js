const fetchDataService = async (reqBody, token) => {
  
  try {
    // call api
      let headers = {
        'Content-type': 'application/json'
      }
      if(token !== null) {
        headers = {
          'Content-type': 'application/json',
          'Authorization': `Bearer ${token}`
        }
      }
      const response = await fetch('http://localhost:5001/', {
        method: 'POST',
        body: JSON.stringify(reqBody),
        headers
      })
      const data = await response.json()
      if (data['data'] === null && data['errors']) {
        return data['errors'][0]['message']
      }
      return data['data']
      // finally it's promise to use
  } catch (error) {
    console.log(error)
  }
}

export default fetchDataService