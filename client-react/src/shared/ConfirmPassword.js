const MustMatch = (controlName, matchControlName) => {
  return (formGroup) => {
    const control = formGroup.controls[controlName]
    const matchingControl = formGroup.controls[matchControlName]
    if (matchingControl.errors && !matchingControl.errors.mushMatch) {
      return
    }
    if (control.value !== matchingControl.value) {
      matchingControl.setErrors({ mustMatch: true })
    } else {
      matchingControl.setErrors(null)
    }
  }
}

export {
  MustMatch
}