import React, { Component } from 'react'
import { Link } from 'react-router-dom'
import styles from './Login.scss'

import fetchDataService from '../../services/FetchDataServices'

import { withRouter } from "react-router-dom";

// react reactive form
import {
  FormBuilder,
  FieldGroup,
  FieldControl,
  Validators,
} from "react-reactive-form";

import TextInput from '../../components/TextInput/TextInput'
import HeaderGeneral from '../../components/HeaderGeneral/HeaderGeneral'

// material ui components
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';


export default class Login extends Component {
  // login form init
  loginForm = FormBuilder.group({
    username: [
      '',
      [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30),
        Validators.pattern(/(^[a-zA-Z0-9_]*$)/)
      ]
    ],
    password: [
      '',
      [
        Validators.required,
        Validators.minLength(10)
      ]
    ]
  })

  login = (e) => {
    e.preventDefault();

    const {username, password } = this.loginForm.value

    // grab data
    const reqBody = {
      query: `
        mutation {
          login(username: "${username}", password: "${password}")
        }
      `
    }

    // after login, set token and navigate to todos page
    fetchDataService(reqBody, null).then(data => {
      localStorage.setItem('token', data.login)
      this.props.history.push('/todos')
    }, error => {
      console.log(error)
    })

  }

  render() {
    return (
    <>
      <HeaderGeneral />
      <Container className={ styles.container }>
        <h1>Login</h1>
        <FieldGroup
          control={ this.loginForm }
          render={ ({ get, invalid } ) => (
            <form onSubmit={this.login}>
              <FieldControl
                name="username"
                render={ TextInput }
                meta={{
                  label: "Username",
                  id: 'username',
                  type: 'text',
                  errors: [
                    { required: 'is required' },
                    { minLength: 'should be at least 2 letters' },
                    { maxLength: 'should not exceed 30 letters' },
                    { pattern: 'should be a-z or A-Z, or 0-9' }
                  ]
                }}
              />    
              <FieldControl
                name="password"
                render={ TextInput }
                meta={{
                  label: "Password",
                  id: 'password',
                  type: 'password',
                  errors: [
                    { required: 'is required' },
                    { minLength: 'should be at least 10 letters' }
                  ]
                }}
              />
              {/* form-row */}
              <p>Have not account?
                <Link to="/register">Join us!</Link>
              </p>
              <Button type="submit" variant="contained" color="primary" disableElevation disabled={invalid}>
                Login
              </Button>
            </form>
          )}
          />
      </Container>
    </>
    )
  }
}

// here need for push navigation to todos page
Login = withRouter(Login);