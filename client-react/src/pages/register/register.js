import  React, { Component } from 'react'
import { withRouter, Link } from 'react-router-dom'
import styles from './Register.scss'

import fetchDataService from '../../services/FetchDataServices'

// react reactive form
import {
  FormBuilder,
  FieldGroup,
  FieldControl,
  Validators,
} from "react-reactive-form";

import TextInput from '../../components/TextInput/TextInput'
import HeaderGeneral from '../../components/HeaderGeneral/HeaderGeneral'

// material ui components
import Button from '@material-ui/core/Button';
import Container from '@material-ui/core/Container';

import { MustMatch } from '../../shared/ConfirmPassword'


export default class Register extends Component {
  // register form init
  registerForm = FormBuilder.group({
    username: [
      '',
      [
        Validators.required,
        Validators.minLength(2),
        Validators.maxLength(30),
        Validators.pattern(/(^[a-zA-Z0-9_]*$)/)
      ]
    ],
    password: [
      '',
      [
        Validators.required,
        Validators.minLength(10)
      ]
    ],
    confirmpassword: [
      '',
      Validators.required,
    ]
  }, {
    validators: MustMatch('password', 'confirmpassword')
  })



  register = (e) => {
    e.preventDefault();
    const { username, password } = this.registerForm.value

    // grab data
    const reqBody = {
      query: `
        mutation {
          signup(username: "${username}", password: "${password}")
        }
      `
    }

    // After register, set token and navigate to todos page
    fetchDataService(reqBody, null).then(data => {
      if(!data.signup) {
        console.log(data)
        return
      }
      localStorage.setItem('token', data.signup)
      this.props.history.push('/todos')
    }, error => {
      console.log(error)
    })
  }

  render() {

    return(
      <>
      <HeaderGeneral />
      <Container className={ styles.container }>
          <h1>Register</h1>
          <FieldGroup
            control={ this.registerForm }
            render={ ({ get, invalid } ) => (
              <form onSubmit={this.register}>
                <FieldControl
                  name="username"
                  render={ TextInput }
                  meta={{
                    label: "Username",
                    id: 'username',
                    type: 'text',
                    errors: [
                      { required: 'is required' },
                      { minLength: 'should be at least 2 letters' },
                      { maxLength: 'should not exceed 30 letters' },
                      { pattern: 'should be a-z or A-Z, or 0-9' }
                    ]
                  }}
                />    
                <FieldControl
                  name="password"
                  render={ TextInput }
                  meta={{
                    label: "Password",
                    id: 'password',
                    type: 'password',
                    errors: [
                      { required: 'is required' },
                      { minLength: 'should be at least 10 letters' }
                    ]
                  }}
                />
                <FieldControl
                  name="confirmpassword"
                  render={ TextInput }
                  meta={{
                    label: "Confirm Password",
                    id: 'confirmpassword',
                    type: 'password',
                    errors: [
                      { required: 'is required' },
                      { mustMatch: 'Password must match' }
                    ]
                  }}
                />
                {/* form-row */}
                <p>Have account already?
                  <Link to="/login">Login here!</Link>
                </p>
                <Button type="submit" variant="contained" color="primary" disableElevation disabled={invalid}>
                  Register
                </Button>
              </form>
            )}
            />
        </Container>
      </>
    )
  }
}

Register = withRouter(Register)